public class Questao1 {

	public static void main(String[] args) {
		String[] entrada = new String[1000];
		int posicao = 0;
		boolean isEOF = false;

		do {
			entrada[posicao] = MyIO.readLine();	    
			isEOF = isEOF(entrada[posicao]);
			posicao++;
		} while ( isEOF == false );
		posicao--; // Desconsiderar a ultima linha, por conter "FIM"

		for (int i = 0; i < posicao; i++) {
			boolean isPalindromo = isPalindromo(entrada[i]);
			String saida = isPalindromo ? "SIM" : "NAO";
			MyIO.println(saida);
		}
	}

	public static boolean isEOF(String palavra) {
		String flagEOF = "FIM";
		boolean isEOF = true;

		if (palavra.length() == flagEOF.length()) {
			for (int i = 0; i < palavra.length(); i++) {
				if (palavra.charAt(i) != flagEOF.charAt(i)) {
					return false;
				}
			}    
		} else {
			isEOF = false;
		}

		return isEOF;
	}

	public static boolean isPalindromo(String s) {
		return isPalindromo(s, 0);
	}	

	public static boolean isPalindromo(String s, int i) {
		boolean retorno = true;
		
		int j = s.length() - 1;

		if (i == (s.length() / 2)) {
			retorno = true;
		} else if (s.charAt(i) == s.charAt(j - i)) {
			retorno = isPalindromo(s, i + 1);
		} else {
			retorno = false;
		}

		return retorno;
	}
}
