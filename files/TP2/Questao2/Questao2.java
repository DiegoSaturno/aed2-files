public class Questao2 {

	public static void main(String[] args) {
		String[] entrada = new String[1000];
		int posicao = 0;
		boolean isEOF = false;

		do {
			entrada[posicao] = MyIO.readLine();	    
			isEOF = isEOF(entrada[posicao]);
			posicao++;
		} while ( isEOF == false );
		posicao--; // Desconsiderar a ultima linha, por conter "FIM"

		int key = 3;
		for (int i = 0; i < posicao; i++) {
			String saida = cifrarPalavra(entrada[i], key);			
			MyIO.println(saida);
		}
	}

	public static boolean isEOF(String palavra) {
		String flagEOF = "FIM";
		boolean isEOF = true;

		if (palavra.length() == flagEOF.length()) {
			for (int i = 0; i < palavra.length(); i++) {
				if (palavra.charAt(i) != flagEOF.charAt(i)) {
					return false;
				}
			}    
		} else {
			isEOF = false;
		}

		return isEOF;
	}

	public static String cifrarPalavra(String s, int key) {
		return cifrarPalavra(s, key, 0);
	}

	public static String cifrarPalavra(String s, int key, int i) {		
		String retorno = "";

		if (i == s.length()) {
			retorno = "";
		} else {
			char c = (char) ((int) s.charAt(i) + key);	
			retorno = (char) c + cifrarPalavra(s, key, i + 1);
		}		

		return retorno;
	}
}
