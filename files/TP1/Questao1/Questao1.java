public class Questao1 {

	public static void main(String[] args) {
		String[] entrada = new String[1000];
		int posicao = 0;
		boolean isEOF = false;

		do {
			entrada[posicao] = MyIO.readLine();	    
			isEOF = isEOF(entrada[posicao]);
			posicao++;
		} while ( isEOF == false );
		posicao--; // Desconsiderar a ultima linha, por conter "FIM"

		for (int i = 0; i < posicao; i++) {
			boolean isPalindromo = isPalindromo(entrada[i]);
			String saida = isPalindromo ? "SIM" : "NAO";
			MyIO.println(saida);
		}
	}

	public static boolean isEOF(String palavra) {
		String flagEOF = "FIM";
		boolean isEOF = true;

		if (palavra.length() == flagEOF.length()) {
			for (int i = 0; i < palavra.length(); i++) {
				if (palavra.charAt(i) != flagEOF.charAt(i)) {
					return false;
				}
			}    
		} else {
			isEOF = false;
		}

		return isEOF;
	}
	
	public static boolean isPalindromo(String s) {
		boolean retorno = true;
		
		int j = s.length();

		for (int i = 0; i < s.length(); i++) {
			char c1 = s.charAt(i);
			char c2 = s.charAt(j - 1);

			if (c1 != c2) {
				retorno = false;
				i = s.length();
			}
			j--;
		}

		return retorno;
	}
}
