/**
 * Classe Arvore binaria de municipios.
 * @author Diego Oliveira
 */
class No {
	private Municipio municipio;
	public No esq, dir;
	public int nivel;

	No(Municipio m) {		
		this(null, null, m, 1);
	}
	
	No(No esq, No dir, Municipio m, int nivel) {
		this.esq = esq;
		this.dir = dir;
		this.nivel = nivel;
		this.setMunicipio(m);
	}

	public void setMunicipio(Municipio m) {
		this.municipio = m.clone();
	}

	public Municipio getMunicipio() {
		return this.municipio;
	}

	public No setNivel() {
		this.nivel = 1 + Math.max(this.getNivel(esq), this.getNivel(dir));
		return this;
	}

	public static int getNivel(No no) {
		return (no == null) ? 0 : no.nivel;
	}
}

class Arvore {
	private No raiz;
	private int numComparacoes;


	/**
	 * Construtor da classe.
	 */
	public Arvore() {
		this.raiz = null;
		this.setNumComparacoes(0);
	}

	public void setNumComparacoes(int x) {
		this.numComparacoes = x;
	}

	public int getNumComparacoes() {
		return this.numComparacoes;
	}
	
	/**
	 * Insere um elemento no inicio da lista.
	 * @param m municipio a ser inserido na lista.
	 */
	public void inserir(Municipio m) throws Exception {
		raiz = this.inserir(m, raiz);
	}

	/**
	 * Metodo privado para inserir um elemento.
	 * @param m Municipio a ser inserido.
	 * @param i No a ser verificado para insercao.
	 * @return i No verificado, alterado ou nao.
	 * @throws Exception Se o elemento ja existir.
	 */
	private No inserir(Municipio m, No i) throws Exception {
		if (i == null) {
			i = new No(m.clone());
		} else if (m.getID() < i.getMunicipio().getID()) {
			i.esq = this.inserir(m, i.esq);
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else if (m.getID() > i.getMunicipio().getID()) {
			i.dir = this.inserir(m, i.dir);
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		}

		i = this.balancear(i);
		return i;
	}
	
	private No balancear(No no) {
		if (no != null) {
			int fator = No.getNivel(no.dir) - No.getNivel(no.esq);

			if (Math.abs(fator) <= 1) {
				no = no.setNivel();
			} else if (fator == 2) {
				int fatorFilhoDir = No.getNivel(no.dir.dir) - No.getNivel(no.dir.esq);

				if (fatorFilhoDir == -1) {
					no.dir = this.rotacionarDir(no.dir);
				}

				no = rotacionarEsq(no);
			} else if (fator == -2) {
				int fatorFilhoEsq = No.getNivel(no.esq.dir) - No.getNivel(no.esq.esq);

				if (fatorFilhoEsq == 1) {
					no.esq = this.rotacionarEsq(no.esq);
				}

				no = rotacionarDir(no);
			}
		}

		return no;
	}

	private No rotacionarDir(No no) {
		No noEsq = no.esq;
		No noEsqDir = noEsq.dir;

		noEsq.dir = no;
		no.esq = noEsqDir;

		no.setNivel();
		noEsq.setNivel();

		return noEsq;
	}

	private No rotacionarEsq(No no) {
		No noDir = no.dir;
		No noDirEsq = noDir.esq;

		noDir.esq = no;
		no.dir = noDirEsq;
		
		no.setNivel();
		noDir.setNivel();

		return noDir;
	}
	
	/**
	 * Metodo iterativo para remocao de municipio.
	 * @param id A chave de remocao do elemento.
	 */
	public void remover(int id) throws Exception {
		raiz = remover(id, raiz);
	}

	/**
	 * Metodo privado para remocao do municipio
	 * @param id A chave de pesquisa da remocao.
	 * @param i No em analise.
	 * @return No verificado, modificado ou nao.
	 */
	private No remover(int id, No i) throws Exception {
		if (i == null) {
			//throw new Exception("Erro ao remover");
		} else if (id < i.getMunicipio().getID()) {
			i.esq = this.remover(id, i.esq);
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else if  (id > i.getMunicipio().getID()) {
			i.dir = this.remover(id, i.dir);
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else if (i.dir == null) {
			i = i.esq;
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else if (i.esq == null) {
			i = i.dir;
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else {
			i.esq = this.antecessor(i, i.esq);
		}

		i = this.balancear(i);
		return i;
	}

	private No antecessor(No i, No j) {
		if (j.dir != null) {
			j.dir = antecessor(i, j.dir);
		} else {
			i.setMunicipio(j.getMunicipio());
			j = j.esq;
		}

		return j;
	}

	/**
	 * Verifica a existencia de um municipio com o id indicado por parametro.
	 * @param id ID do município a ser pesquisado.
	 * @return <code>true</code> se o municipio existir, <code>false</code> caso contrario.
	 */
	public boolean pesquisar(int id) {
		MyIO.print("raiz ");
		return pesquisar(id, raiz);
	}

	private boolean pesquisar(int id, No i) {
		boolean resp = false;
		
		if (i == null) {
			resp = false;
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else if (id == i.getMunicipio().getID()) {
			resp = true;
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else if (id < i.getMunicipio().getID()) {
			MyIO.print("esq ");
			resp = this.pesquisar(id, i.esq);
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		} else if (id > i.getMunicipio().getID()) {
			MyIO.print("dir ");
			resp = this.pesquisar(id, i.dir);
			this.setNumComparacoes(this.getNumComparacoes() + 1);
		}

		return resp;
	}

	public void mostrar() {
		this.mostrar(raiz);
	}

	private void mostrar(No i) {
		if (i != null) {
			this.mostrar(i.esq);
			i.getMunicipio().imprimir();
			this.mostrar(i.dir);
		}
	}
}

class Municipio {

	//Atributos
	private int id;
	private String nome;
	private String UF;
	private int codigoUF;
	private int populacao;
	private int numFuncionarios;
	private int numComissionados;
	private String escolaridade;
	private String estagio;
	private int attPlano;
	private String regiao;
	private int attCadastro;
	private boolean isConsorcio;

	Municipio() {
		this(0, "", "", 0, 0, 0, 0, "", "", 0, "", 0, false);
	}

	Municipio(int id, String nome, String UF, int codigoUF, int populacao, int numFuncionarios, int numComissionados, String escolaridade, String estagio, int attPlano, String regiao, int attCadastro, boolean isConsorcio) {

		this.setID(id);
		this.setNome(nome);
		this.setUF(UF);
		this.setCodigoUF(codigoUF);		
		this.setPopulacao(populacao);
		this.setNumFuncionarios(numFuncionarios);
		this.setNumComissionados(numComissionados);
		this.setEscolaridade(escolaridade);
		this.setEstagio(estagio);
		this.setAttPlano(attPlano);
		this.setIsConsorcio(isConsorcio);
	}

	//Getters e Setters

	public void setID(int id) {
		this.id = id;
	}

	public int getID() {
		return this.id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return this.nome;
	}

	public void setUF(String UF) {
		this.UF = UF;
	}

	public String getUF() {
		return this.UF;
	}

	public void setCodigoUF(int codigoUF) {
		this.codigoUF = codigoUF;
	}

	public int getCodigoUF() {
		return this.codigoUF;
	}

	public void setPopulacao(int populacao) {
		this.populacao = populacao;
	}

	public int getPopulacao() {
		return this.populacao;
	}

	public void setNumFuncionarios(int numFuncionarios) {
		this.numFuncionarios = numFuncionarios;
	}

	public int getNumFuncionarios() {
		return this.numFuncionarios;
	}

	public void setNumComissionados(int numComissionados) {
		this.numComissionados = numComissionados;
	}

	public int getNumComissionados() {
		return this.numComissionados;
	}

	public void setEscolaridade(String escolaridade) {
		this.escolaridade = escolaridade;
	}

	public String getEscolaridade() {
		return this.escolaridade;
	}

	public void setEstagio(String estagio) {
		this.estagio = estagio;
	}

	public String getEstagio() {
		return this.estagio;
	}

	public void setAttPlano(int attPlano) {
		this.attPlano = attPlano;
	}

	public int getAttPlano() {
		return this.attPlano;
	}

	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}

	public String getRegiao() {
		return this.regiao;
	}

	public void setAttCadastro(int attCadastro) {
		this.attCadastro = attCadastro;
	}

	public int getAttCadastro() {
		return this.attCadastro;
	}

	public void setIsConsorcio(boolean isConsorcio) {
		this.isConsorcio = isConsorcio;
	}

	public boolean getIsConsorcio() {
		return this.isConsorcio;
	}

	public Municipio clone() {
		Municipio clone = new Municipio();

		clone.setID(this.getID());
		clone.setNome(this.getNome());
		clone.setUF(this.getUF());
		clone.setCodigoUF(this.getCodigoUF());
		clone.setPopulacao(this.getPopulacao());
		clone.setNumFuncionarios(this.getNumFuncionarios());
		clone.setNumComissionados(this.getNumComissionados());
		clone.setEscolaridade(this.getEscolaridade());
		clone.setEstagio(this.getEstagio());
		clone.setAttPlano(this.getAttPlano());
		clone.setRegiao(this.getRegiao());
		clone.setAttCadastro(this.getAttCadastro());
		clone.setIsConsorcio(this.getIsConsorcio());

		return clone;
	}

	public void imprimir() {
		MyIO.println(
			this.getID() + " " +
			this.getNome() + " " +
			this.getUF() + " " +
			this.getCodigoUF() + " " +
			this.getPopulacao() + " " +
			this.getNumFuncionarios() + " " +
			this.getNumComissionados() + " " +
			this.getEscolaridade() + " " +
			this.getEstagio() + " " +
			this.getAttPlano() + " " +
			this.getRegiao() +  " " +
			this.getAttCadastro() + " " +
			this.getIsConsorcio() + ""
		);
	}

	public void ler(int registro) {
		String charset = "ISO-8859-1"; //ISO-8859-1
		int i = 0;
		String linha;

		//Arquivo 1
		String arq = "/tmp/articulacaoointerinstitucional.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq1Data = linha.split("\t");

		//Variaveis a serem extraidas do arquivo
		int id = (this.isInteger(arq1Data[0])) ? Integer.parseInt(arq1Data[0]) : 0;
		int codigoUF = Integer.parseInt(arq1Data[1]);
		String nome = arq1Data[3];


		boolean isConsorcio = (arq1Data[5].equals("Sim")) ? true : false;

		Arq.close();

		//Arquivo 2
		arq = "/tmp/gestaoambiental.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq2Data = linha.split("\t");
		String estagio = arq2Data[7];

		Arq.close();

		//Arquivo 3
		arq = "/tmp/planejamentourbano.txt";		
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq3Data = linha.split("\t");

		String escolaridade = arq3Data[5];
		int attPlano = (this.isInteger(arq3Data[8])) ? Integer.parseInt(arq3Data[8]) : 0;

		Arq.close();

		//Arquivo 4
		arq = "/tmp/recursoshumanos.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq4Data = linha.split("\t");

		int numFuncionarios = this.isInteger(arq4Data[7]) ? Integer.parseInt(arq4Data[4]) : 0;
		int numComissionados = (this.isInteger(arq4Data[7])) ? Integer.parseInt(arq4Data[7]) : 0;

		Arq.close();

		//Arquivo 5
		arq = "/tmp/recursosparagestao.txt";
		Arq.openRead(arq, charset);
		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq5Data = linha.split("\t");

		int attCadastro = this.isInteger(arq5Data[6]) ? Integer.parseInt(arq5Data[6]) : 0;

		Arq.close();

		//Arquivo 6
		arq = "/tmp/terceirizacaoeinformatizacao.txt";		
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq6Data = linha.split("\t");

		Arq.close();

		//Arquivo 7
		arq = "/tmp/variaveisexternas.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq7Data = linha.split("\t");

		String regiao = arq7Data[1];
		int populacao = this.isInteger(arq7Data[6]) ? Integer.parseInt(arq7Data[6]) : 0;
		String UF = arq7Data[3];

		Arq.close();

		//Setando os atributos
		this.setID(id);
		this.setNome(nome);
		this.setUF(UF);
		this.setCodigoUF(codigoUF);
		this.setPopulacao(populacao);
		this.setNumFuncionarios(numFuncionarios);
		this.setNumComissionados(numComissionados);
		this.setEscolaridade(escolaridade);
		this.setEstagio(estagio);
		this.setAttPlano(attPlano);
		this.setRegiao(regiao);
		this.setAttCadastro(attCadastro);
		this.setIsConsorcio(isConsorcio);
	}

	public boolean isInteger(String p) {
		boolean isInt = false;
		try {
			int number = Integer.parseInt(p);

			isInt = true;
		} catch (Exception e) {
			isInt = false;
		}

		return isInt;
	}

	public boolean equals(String s1, String s2) {
		boolean equals = false;		
		
		if (s1.length() == s2.length()) {
			for (int i = 0; i < s1.length(); i++) {
				MyIO.println(s1.charAt(i) + " " + s2.charAt(i));
			}
		}

		return equals;
	}
}

public class Questao3 {
	public static void main(String[] args) throws Exception {

		Arvore ab = new Arvore();
		MyIO.setCharset("ISO-8859-1");

		for (int linha = MyIO.readInt(); linha != 0; linha = MyIO.readInt()) {
			Municipio m = new Municipio();
			m.ler(linha);
			ab.inserir(m);
		}

		int contador = MyIO.readInt();
		
		long inicio = System.currentTimeMillis();
			
		for (int i = 0; i < contador; i++) {
			String comando = MyIO.readString();
			Municipio mu = new Municipio();

			if (comando.charAt(0) == 'I') {
				int registro = MyIO.readInt();
				mu.ler(registro);
				ab.inserir(mu);
			} else if (comando.charAt(0) == 'R') {
				int id = MyIO.readInt();
				ab.remover(id);
			}
		}
		
		for (String linha1 = MyIO.readLine(); linha1.equals("FIM") == false; linha1 = MyIO.readLine()) {
			int id = Integer.parseInt(linha1);

			boolean exists = ab.pesquisar(id);
			String saida = exists ? "SIM" : "NAO";
			MyIO.println(saida);
		}

		long fim = System.currentTimeMillis();
		long diff = (fim - inicio) / 1000;

		writeLog(diff, ab.getNumComparacoes());
	}

	public static void writeLog(long time, int comparacoes) {
		Arq.openWrite("matricula_avl.txt");
		Arq.print("561325\t");
		Arq.print(time + "\t" + comparacoes);
		Arq.close();
	}
}
