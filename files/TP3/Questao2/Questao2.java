/**
 * Classe lista com alocação estática.
 * @author Diego Oliveira
 */

class Lista {
	private Municipio[] array;
	private int n;
	
	/**
	 * Construtor da classe.
	 *
	 */
	public Lista() {
		this(6000);
	}
	
	/**
	 * construtor da classe.
	 * @param tamanho tamanho da lista
	 */
	public Lista(int tamanho) {
		array = new Municipio[tamanho];
		n = 0;
	}
	
	/**
	 * Insere um elemento no início da lista.
	 * @param m Municipio a ser inserido na lista
	 * @throws Exception se a lista estiver cheia.
	 */
	public void inserirInicio(Municipio m) throws Exception {
		if(n >= array.length) {
			throw new Exception("Erro ao inserir: lista cheia!");
		}

		for (int i = n; i > 0; i--) {
			array[i] = array[i - 1].clone();
		}

		array[0] = m.clone();
		n++;
	}

	/**
	 * Insere um municipio no fim da lista.
	 * @param m Municipio a ser inserido.
	 * @throws Exception se a lista estiver cheia.
	 */
	public void inserirFim(Municipio m) throws Exception {
		if (n >= array.length) { 
			throw new Exception("Erro ao inserir");
		}

		array[n] = m.clone();
		n++;
	}

	/**
	 * Insere um municipio em uma posição específica da lista, e move os demais para o fim da lista.
	 * @param m Municipio a ser inserido.
	 * @throws Exception se a lista estiver cheia ou se a posição for inválida.
	 */
	public void inserir(Municipio m, int pos) throws Exception {
		
		if (n >= array.length || pos < 0 || pos > n) {
			throw new Exception("Erro ao inserir.");
		}

		for (int i = n; i > pos; i--) {
			array[i] = array[i - 1].clone();
		}

		array[pos] = m.clone();
		n++;
	}

	/**
	 * Remove um elemento da primeira posição da lista, e move os demais para o inicio.
	 * @return mRemovido municipio a ser removido
	 * @throws Exception se a lista estiver vazia.
	 */
	public Municipio removerInicio() throws Exception {
		
		if (n == 0) {
			throw new Exception("Erro ao remover. A lista está vazia.");
		}

		Municipio mRemovido = array[0].clone();
		n--;
		
		for (int i = 0; i < n; i++) {
			array[i] = array[i + 1].clone();
		}

		return mRemovido;
	}

	/**
	 * Remove um elemento da última posição da lista.
	 * @return mRemovido municipio a ser removido da lista.
	 * @throws Exception se a lista estiver vazia.
	 */
	public Municipio removerFim() throws Exception {
		
		if (n == 0) {
			throw new Exception("Erro ao remover. A lista está vazia");
		}

		return array[--n].clone();
	}

	/**
	 * Remove um elemento de uma posição da lista, e move os demais para o inicio.
	 * @param pos posição do elemento a ser removido.
	 * @throws Exception se a posição for inválida ou a lista estiver vazia.
	 */
	public Municipio remover(int pos) throws Exception {

		if (pos >= n || n == 0 || pos < 0) {
			throw new Exception("Erro ao remover: posição inválida");
		}

		Municipio mRemovido = array[pos].clone();
		n--;		

		for (int i = pos; i < n; i++) {
			array[i] = array[i + 1].clone();
		}
		
		return mRemovido;
	}

	/**
	 * Pesquisa a existencia de um elemento na lista.
	 * @param id o id do elemento
	 * @return <code>true</code> se o elemento existir,
	 * <code>false</code> caso não exista.
	 */
	public boolean pesquisar(int id) {
		boolean exists = false;
		for (int i = 0; i < n && exists == false; i++) {
			exists = (array[i].getID() == n) ? true : false;
		}
	
		return exists;
	}

	/**
	 * Mostra todos os elementos da lista, em sua ordem de inserção.
	 */

	public void mostrar() {
		for (int i = 0; i < n; i++) {
			array[i].imprimir();
		}
	}

	public void mostrar(int pos) {
		if (pos > 0 && pos < n) {
			array[pos].imprimir();
		}
	}

	public int getNumeroElementos() {
		return this.n;
	}
}

class Municipio {

	//Atributos
	private int id;
	private String nome;
	private String UF;
	private int codigoUF;
	private int populacao;
	private int numFuncionarios;
	private int numComissionados;
	private String escolaridade;
	private String estagio;
	private int attPlano;
	private String regiao;
	private int attCadastro;
	private boolean isConsorcio;

	Municipio() {
		this(0, "", "", 0, 0, 0, 0, "", "", 0, "", 0, false);
	}

	Municipio(int id, String nome, String UF, int codigoUF, int populacao, int numFuncionarios, int numComissionados, String escolaridade, String estagio, int attPlano, String regiao, int attCadastro, boolean isConsorcio) {

		this.setID(id);
		this.setNome(nome);
		this.setUF(UF);
		this.setCodigoUF(codigoUF);		
		this.setPopulacao(populacao);
		this.setNumFuncionarios(numFuncionarios);
		this.setNumComissionados(numComissionados);
		this.setEscolaridade(escolaridade);
		this.setEstagio(estagio);
		this.setAttPlano(attPlano);
		this.setIsConsorcio(isConsorcio);
	}

	//Getters e Setters

	public void setID(int id) {
		this.id = id;
	}

	public int getID() {
		return this.id;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNome() {
		return this.nome;
	}

	public void setUF(String UF) {
		this.UF = UF;
	}

	public String getUF() {
		return this.UF;
	}

	public void setCodigoUF(int codigoUF) {
		this.codigoUF = codigoUF;
	}

	public int getCodigoUF() {
		return this.codigoUF;
	}

	public void setPopulacao(int populacao) {
		this.populacao = populacao;
	}

	public int getPopulacao() {
		return this.populacao;
	}

	public void setNumFuncionarios(int numFuncionarios) {
		this.numFuncionarios = numFuncionarios;
	}

	public int getNumFuncionarios() {
		return this.numFuncionarios;
	}

	public void setNumComissionados(int numComissionados) {
		this.numComissionados = numComissionados;
	}

	public int getNumComissionados() {
		return this.numComissionados;
	}

	public void setEscolaridade(String escolaridade) {
		this.escolaridade = escolaridade;
	}

	public String getEscolaridade() {
		return this.escolaridade;
	}

	public void setEstagio(String estagio) {
		this.estagio = estagio;
	}

	public String getEstagio() {
		return this.estagio;
	}

	public void setAttPlano(int attPlano) {
		this.attPlano = attPlano;
	}

	public int getAttPlano() {
		return this.attPlano;
	}

	public void setRegiao(String regiao) {
		this.regiao = regiao;
	}

	public String getRegiao() {
		return this.regiao;
	}

	public void setAttCadastro(int attCadastro) {
		this.attCadastro = attCadastro;
	}

	public int getAttCadastro() {
		return this.attCadastro;
	}

	public void setIsConsorcio(boolean isConsorcio) {
		this.isConsorcio = isConsorcio;
	}

	public boolean getIsConsorcio() {
		return this.isConsorcio;
	}

	public Municipio clone() {
		Municipio clone = new Municipio();

		clone.setID(this.getID());
		clone.setNome(this.getNome());
		clone.setUF(this.getUF());
		clone.setCodigoUF(this.getCodigoUF());
		clone.setPopulacao(this.getPopulacao());
		clone.setNumFuncionarios(this.getNumFuncionarios());
		clone.setNumComissionados(this.getNumComissionados());
		clone.setEscolaridade(this.getEscolaridade());
		clone.setEstagio(this.getEstagio());
		clone.setAttPlano(this.getAttPlano());
		clone.setRegiao(this.getRegiao());
		clone.setAttCadastro(this.getAttCadastro());
		clone.setIsConsorcio(this.getIsConsorcio());

		return clone;
	}

	public void imprimir() {
		MyIO.println(
			this.getID() + " " +
			this.getNome() + " " +
			this.getUF() + " " +
			this.getCodigoUF() + " " +
			this.getPopulacao() + " " +
			this.getNumFuncionarios() + " " +
			this.getNumComissionados() + " " +
			this.getEscolaridade() + " " +
			this.getEstagio() + " " +
			this.getAttPlano() + " " +
			this.getRegiao() +  " " +
			this.getAttCadastro() + " " +
			this.getIsConsorcio() + ""
		);
	}

	public void ler(int registro) {
		String charset = "ISO-8859-1"; //ISO-8859-1
		int i = 0;
		String linha;

		//Arquivo 1
		String arq = "/tmp/articulacaoointerinstitucional.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq1Data = linha.split("\t");

		//Variáveis a serem extraídas do arquivo
		int id = (this.isInteger(arq1Data[0])) ? Integer.parseInt(arq1Data[0]) : 0;
		int codigoUF = Integer.parseInt(arq1Data[1]);
		String nome = arq1Data[3];

		/////////////DEBUGAR ESTA JOÇA!

		boolean isConsorcio = (arq1Data[5].equals("Sim")) ? true : false;

		Arq.close();

		//Arquivo 2
		arq = "/tmp/gestaoambiental.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq2Data = linha.split("\t");
		String estagio = arq2Data[7];

		Arq.close();

		//Arquivo 3
		arq = "/tmp/planejamentourbano.txt";		
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq3Data = linha.split("\t");

		String escolaridade = arq3Data[5];
		int attPlano = (this.isInteger(arq3Data[8])) ? Integer.parseInt(arq3Data[8]) : 0;

		Arq.close();

		//Arquivo 4
		arq = "/tmp/recursoshumanos.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq4Data = linha.split("\t");

		int numFuncionarios = this.isInteger(arq4Data[7]) ? Integer.parseInt(arq4Data[4]) : 0;
		int numComissionados = (this.isInteger(arq4Data[7])) ? Integer.parseInt(arq4Data[7]) : 0;

		Arq.close();

		//Arquivo 5
		arq = "/tmp/recursosparagestao.txt";
		Arq.openRead(arq, charset);
		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq5Data = linha.split("\t");

		int attCadastro = this.isInteger(arq5Data[6]) ? Integer.parseInt(arq5Data[6]) : 0;

		Arq.close();

		//Arquivo 6
		arq = "/tmp/terceirizacaoeinformatizacao.txt";		
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq6Data = linha.split("\t");

		Arq.close();

		//Arquivo 7
		arq = "/tmp/variaveisexternas.txt";
		Arq.openRead(arq, charset);

		for (i = 0, linha = Arq.readLine(); i < registro; linha = Arq.readLine(), i++);

		String[] arq7Data = linha.split("\t");

		String regiao = arq7Data[1];
		int populacao = this.isInteger(arq7Data[6]) ? Integer.parseInt(arq7Data[6]) : 0;
		String UF = arq7Data[3];

		Arq.close();

		//Setando os atributos
		this.setID(id);
		this.setNome(nome);
		this.setUF(UF);
		this.setCodigoUF(codigoUF);
		this.setPopulacao(populacao);
		this.setNumFuncionarios(numFuncionarios);
		this.setNumComissionados(numComissionados);
		this.setEscolaridade(escolaridade);
		this.setEstagio(estagio);
		this.setAttPlano(attPlano);
		this.setRegiao(regiao);
		this.setAttCadastro(attCadastro);
		this.setIsConsorcio(isConsorcio);
	}

	public boolean isInteger(String p) {
		boolean isInt = false;
		try {
			int number = Integer.parseInt(p);

			isInt = true;
		} catch (Exception e) {
			isInt = false;
		}

		return isInt;
	}

	public boolean equals(String s1, String s2) {
		boolean equals = false;		
		
		if (s1.length() == s2.length()) {
			for (int i = 0; i < s1.length(); i++) {
				MyIO.println(s1.charAt(i) + " " + s2.charAt(i));
			}
		}

		return equals;
	}
}

public class Questao2 {
	public static void main(String[] args) throws Exception {

		Lista lista = new Lista(1000);
		MyIO.setCharset("ISO-8859-1");

		for (int linha = MyIO.readInt(); linha != 0; linha = MyIO.readInt()) {
			Municipio m = new Municipio();
			m.ler(linha);
			lista.inserirFim(m);
		}

		int n = MyIO.readInt();
 
		for(int i = 0; i < n; i++){
			String comando = MyIO.readString();

			Municipio municipio = new Municipio();
			if(comando.charAt(0) == 'I'){
				if(comando.charAt(1) == 'I') {
					municipio.ler(MyIO.readInt());
					lista.inserirInicio(municipio);
				} else if (comando.charAt(1) == 'F'){
					municipio.ler(MyIO.readInt());
					lista.inserirFim(municipio);
				} else if (comando.charAt(1) == '*'){
					int pos = MyIO.readInt();
					municipio.ler(MyIO.readInt());
					lista.inserir(municipio, pos);
				} else {
					MyIO.println("Comando invalido: " + comando);
					System.exit(1);
				}
			} else if (comando.charAt(0) == 'R'){				
				if(comando.charAt(1) == 'I'){
					municipio = lista.removerInicio();
				} else if (comando.charAt(1) == 'F'){
					municipio = lista.removerFim();
				} else if (comando.charAt(1) == '*'){	
					municipio = lista.remover(MyIO.readInt());
				} else {
					MyIO.println("Comando invalido: " + comando);
					System.exit(1);
				}
				MyIO.println("(R) " + municipio.getNome());
			} else {
				MyIO.println("Comando invalido: " + comando);
				System.exit(1);
			}
		}

		lista.mostrar();
	}
}
